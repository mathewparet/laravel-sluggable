<?php
namespace mathewparet\LaravelSluggable\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Blueprint;

class SlugServiceProvider extends ServiceProvider
{
    const CONFIG_FILE = __DIR__.'/config/sluggable.php';
 
    public function boot()
    {
        Blueprint::macro("sluggable", function($name = 'slug') {
            /**
             * @var \Illuminate\Database\Schema\Blueprint $this
             */
            return $this->string($name);
        });

        $this->definePublishableComponents();
    }

    public function register()
    {
        $this->mergeConfigFrom(self::CONFIG_FILE, 'sluggable');
    }

    private function definePublishableComponents()
    {
        $this->publishes([
            self::CONFIG_FILE => config_path('sluggable.php')
        ], 'laravel-sluggable');
    }
}